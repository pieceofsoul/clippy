######################################################################
#	name		:	clippy
#	description	:	qt pro file, used for makefile generation
#	filename	:	clippy.pro
#	author		:	vth ( fraunhofer institut iitb )
#	date		:	Fr Mai 12 11:46:08 2006
######################################################################

# exclude default qt includes
# QT -= gui
# QT -= core

DESTDIR = $$PWD/../build

QT += widgets

win32 {
    TEMPLATE = app
}

TARGET = clippy

SOURCES =  main.cpp

RC_FILE = clippy.rc
